<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_stock', function (Blueprint $table) {
            $table->unsignedInteger('stock_id');
            $table->unsignedInteger('resource_id');
            $table->float('quantity');
            $table->timestamp('ts')->default(\DB::raw('CURRENT_TIMESTAMP'));
            
            $table->unique(['stock_id', 'resource_id']);
            $table->index('stock_id');
            $table->index('resource_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_stock');
    }
}
