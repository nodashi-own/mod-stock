@extends('backend')


@section('content')
<table>
    <thead>
        <tr>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
        @foreach($stocks as $item)
        <tr>
            <td><a href="{{ route('stock.detail', ['id'=>$item->id]) }}">{{ $item->sto_name }}</a></td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $stocks->render() }}
<span>Total: {{ $stocks->count() }}</span>
@stop