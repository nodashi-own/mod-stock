@extends('backend')


@section('content')
<div>
    <form method="post" action="{{ route('resource.save') }}">
        {{ csrf_field() }}
        <div>
            <label>Resource</label>            
            <input type="text" name="resource[res_name]" value="{{ $resource->res_name }}">
            <label>Note</label>
            <textarea name="resource[res_note]">{{ $resource->res_note }}</textarea>
        </div>
        <button>Save</button>
    </form>
</div>
@stop