@extends('backend')


@section('content')
<table>
    <thead>
        <tr>
            <th>Stock</th>
            <th>Resource</th>
            <th>Quantity</th>
            <th>Date</th>
        </tr>
    </thead>
    <tbody>
        @foreach($stockmovements as $item)
        <tr>
            <td>{{ $item->sto_name }}</td>
            <td>{{ $item->res_name }}</td>
            <td>{{ $item->stm_quantity }}</td>
            <td>{{ $item->created_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $stockmovements->render() }}
<span>Total: {{ $stockmovements->count() }}</span>
@stop