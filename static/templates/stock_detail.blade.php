@extends('backend')


@section('content')
<div>
    {{ $stock->sto_name }}
</div>
<div>
    {{ $stock->sto_note }}
</div>    
<form method="post" action="{{ route('stock.add.stockitem') }}">
    {{ csrf_field() }}
    <button>Add items</button><input type="text" name="new_count" value="1">
</form>

<h2>Resources</h2>
<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Quantity</th>
        </tr>
    </thead>
    <tbody>
        @foreach($resources as $item)
        <tr>
            <td>{{ $item->res_name }}</td>
            <td>{{ $item->quantity }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $resources->render() }}
Count: {{ $resources->count() }}


@stop