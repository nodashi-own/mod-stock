@extends('backend')


@section('content')
<div>
    <div>
        {{ $resource->res_name }}
        <br>
        {{ $resource->res_note }}
        <br>
        <button><a href="{{ route('resource.edit', ['id'=>$resource->id]) }}">Edit</a></button>
    </div>
    @if($stocks)
    <h2>stocks</h2>
    <table>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Quantity</th>
                </tr>
            </thead>
            <tbody>
                @foreach($stocks as $item)
                <tr>
                    <td>{{ $item->sto_name }}</td>
                    <td>{{ $item->quantity }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </table>
    @endif
</div>
@stop