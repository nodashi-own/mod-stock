@extends('backend')


@section('content')
<div>
    <form method="post" action="{{ route('stock.add.stockitem.save') }}">
        {{ csrf_field() }}
        @for($i=0; $i<$count; $i++)
        <div>
            <label>Stock</label>
            {!! Form::select("items[$i][stock_id]", $stocks) !!}
            {!! Form::select("items[$i][resource_id]", $resources) !!}
            <label>Quantity</label>
            <input type="text" name="items[{{ $i }}][quantity]" value="">
        </div>
        @endfor
        <button>Save</button>
    </form>
</div>
@stop