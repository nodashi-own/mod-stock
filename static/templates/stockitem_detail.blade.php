@extends('backend')


@section('content')
<div>
    <form method="post" action="{{ route('stockitem.save') }}">
        {{ csrf_field() }}
        <div>
            <label>Stock</label>
            {!! Form::select('stockitem[stock_id]', $stocks, $stockitem->stock_id) !!}  
            <label>Resource category</label>
            <input type="text" name="stockitem[sti_name]" value="{{ $stockitem->resourcecategory_id }}">
            <label>Note</label>
            <textarea name="stockitem[sti_name]">{{ $stockitem->sti_note }}</textarea>
        </div>
        <button>Save</button>
    </form>
</div>
@stop