@extends('backend')


@section('content')
<div>
    <table>
        <thead>
            <tr>
                <th>Name</th>
            </tr>
        </thead>
        <tbody
            @foreach($resources as $resource)
            <tr>
                <td><a href="{{ route('resource.detail', ['id'=>$resource->id]) }}">{{ $resource->res_name }}</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop