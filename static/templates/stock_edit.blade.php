@extends('backend')


@section('content')
<div>
    <form method="post" action="{{ route('stock.save') }}">
        {{ csrf_field() }}
        <div>
            <label>Name</label>
            <input type="text" name="stock[sto_name]" value="{{ $stock->sto_name }}">
        </div>
        <div>
            <label>Note</label>
            <textarea name="stock[sto_note]">{{ $stock->sto_note }}</textarea>
        </div>        
        <button>Save</button>
    </form>
</div>
@stop