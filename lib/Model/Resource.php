<?php

namespace ArkSys\Mod\Stock\Model;

use ArkSys\Cache\Events\CacheHasChangedEvent;
use ArkSys\Mod\Resource\Model\Resource as BasicResource;

class Resource extends BasicResource
{
    public function stocks()
    {
        return $this->belongsToMany(Stock::class)
                ->withPivot('quantity');
    }
    
    public static function boot()
    {
        static::saved(function()
        {
            event(new CacheHasChangedEvent());
        });
    }
}
