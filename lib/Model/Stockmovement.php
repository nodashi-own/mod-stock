<?php

namespace ArkSys\Mod\Stock\Model;

use Illuminate\Database\Eloquent\Model;
use ArkSys\Core\Model\Util\SetsModelsUserId;

class Stockmovement extends Model
{
    use SetsModelsUserId;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table='stockmovement';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stm_quantity'
    ];
    
    public function stockitem()
    {
        return $this->belongsTo(Stockitem);
    }
    
    public static function boot()
    {
        static::creating(function(Stockmovement $stockmovement)
        {
            $stockmovement->setUserId();
        });
    }
}
