<?php

namespace ArkSys\Mod\Stock\Model;

use Illuminate\Database\Eloquent\Model;
use ArkSys\Cache\Events\CacheHasChangedEvent;

class Stock extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table='stock';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['sto_name', 'sto_code', 'sto_note'];
    
    
    public function stockitems()
    {
        return $this->hasMany(Stockitem);
    }
    
    public function resources()
    {
        return $this->belongsToMany(Resource::class)
                ->withPivot('quantity');
    }
    
    public static function boot()
    {
        static::saved(function()
        {
            event(new CacheHasChangedEvent());
        });
    }
}
