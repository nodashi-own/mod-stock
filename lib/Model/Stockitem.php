<?php

namespace ArkSys\Mod\Stock\Model;

use Illuminate\Database\Eloquent\Model;

class Stockitem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table='stockitem';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    
    public function stock()
    {
        return $this->belongsTo(Stock);
    }
    
}
