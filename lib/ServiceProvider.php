<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ArkSys\Mod\Stock;

/**
 * Description of ServiceProvider
 *
 * @author nodashi
 */

use Illuminate\Foundation\Support\Providers\EventServiceProvider as Provider;
use ArkSys\Cache\Contracts\ICacheProvider;

class ServiceProvider extends Provider
{
    protected $mod_name = 'mod-stock';
    
    /**
     * register module events and their listeners
     * @var type array
     */
    protected $listen = [
        
    ];        
    
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {    
        parent::boot();        
        
        $this->loadRoutesFrom(__DIR__ . '/../routes.php');
        // $this->loadViewsFrom(__DIR__ . '/../static/templates', self::MOD_NAME);
        // $this->loadTranslationsFrom(__DIR__ . '/../static/lang/', self::MOD_NAME);        
        
        // if(!$this->app->configurationIsCached())
        // {
        //     $this->mergeConfigFrom(__DIR__ . '/../config.php', self::MOD_NAME);
        // }        
        
        $this->loadMigrationsFrom(__DIR__ . '/../migrations');       
    }

//    /**
//     * Register any application services.
//     *
//     * @return void
//     */
    public function register()
    {
        
    }
}
