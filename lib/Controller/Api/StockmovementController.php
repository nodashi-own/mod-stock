<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ArkSys\Mod\Stock\Controller\Api;

/**
 * Description of StockController
 *
 * @author nodashi
 */

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use ArkSys\Mod\Stock\Model\Stock;
use ArkSys\Mod\Stock\Model\Resource;
use ArkSys\Mod\Stock\Model\Stockmovement;
use ArkSys\Mod\Stock\Util\StockManager;

class StockmovementController extends Controller
{       
    public function data()
    {
        $stocks = Stock::all();
        $resources = Resource::all();
        
        return ['stocks'=>$stocks, 'resources'=>$resources];
    }
    
    public function items($stockId=null)
    {
        $query = Stockmovement::join('resource', 'stockmovement.resource_id', '=', 'resource.id')
                //->join('user', 'stockmovement.user_id', '=', 'user.id')
                //->join('user', 'stockmovement.user_id', '=', 'user.id')
                ->select('res_name', 'stm_quantity', 'stockmovement.created_at', 'stm_note');
        
        if($stockId)
        {
            $query->where('stockmovement.stock_id', $stockId);
        }
        
        $items = $query->get();
        return [$items];
    }
    
    public function save(Request $request)
    {
        //return $request->json('data');
        
        $data = $request->json('data');
        $sm = new StockManager();
        
        foreach ($data as $item)
        {   
            $sm->add($item['resource_id'], $item['stock_id'], $item['stm_quantity'], $item['stm_note']);
        }
        
        return ['data'=>'ok'];
    }
}
