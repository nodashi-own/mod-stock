<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ArkSys\Mod\Stock\Controller\Api;

/**
 * Description of StockController
 *
 * @author nodashi
 */

use App\Http\Controllers\Controller;
use ArkSys\Mod\Stock\Model\Stock;
use ArkSys\Mod\Stock\Model\Resource;
use Illuminate\Http\Request;

class StockController extends Controller
{       
    public function find($id)
    {
        if(intval($id)>0)
        {
            $stock = Stock::findOrFail($id);
        }
        else
        {
            abort(404);
        }        
        
        $stock->resources;
        
        return ['stock'=>$stock];
    }

    public function getList()
    {
        $stocks =  Stock::paginate(15);        
        return $stocks;
    }
    
    public function save(Request $request)
    {
        $data = $request->json('data');
        $stock = Stock::findOrNew($data['id']);
        $stock->fill($data);
        $stock->save();
 
        return ['data'=>$stock];
    }   
    
    public function all()
    {
        $stocks = Stock::all();
        return ['data'=>$stocks];
    }
    
    public function items($stockId=null)
    {
        $query = Resource::join('resource_stock', 'resource.id', '=', 'resource_stock.resource_id')
                ->select('resource.id', 'resource.res_name', 'resource_stock.quantity');
        
        if(intval($stockId)>0)
        {
            $query->where('resource_stock.stock_id', $stockId);
        }
        
        return $query->get();
    }

}
