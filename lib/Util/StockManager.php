<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ArkSys\Mod\Stock\Util;

/**
 * Description of StockManager
 *
 * @author nodashi
 */

use ArkSys\Mod\Stock\Model\Stock;
use ArkSys\Mod\Stock\Model\Stockmovement;
use DB;

class StockManager implements IStockManager
{
    public function add($resource, $stock, $quantity, $note=null)
    {
        if($quantity == 0)
        {
            throw new \Exception('Quantity is required.');
        }
                
        $inStock = $this->current($resource, $stock);
        
        if(!$inStock)
        {
            if($quantity<0)
            {
                throw new Exception('Resource not in the stock. Cannot remove.');
            }            
            
            \DB::table('resource_stock')->insert(['stock_id'=>$stock, 'resource_id'=>$resource, 'quantity'=>$quantity]);
            
            return $this->saveMovement($resource, $stock, $quantity, $note);            
        }
        
        if($inStock->quantity + $quantity >= 0)
        {
            $this->saveMovement($resource, $stock, $quantity, $note);
            
            return DB::table('resource_stock')
                    ->where('resource_id', $resource)
                    ->where('stock_id', $stock)
                    ->update(['quantity'=>$inStock->quantity + $quantity]);
            
        }
        
        throw new \Exception('Insufficient resources in the stock. Cannot remove.');
    }
    
    public function remove($resource, $stock, $quantity)
    {
        return $quantity > 0 ? $this->add($resource, $stock, -$quantity) : $this->add($resource, $stock, -$quantity) ;
    }
    
    public function current($resource, $stock=null)
    {
        return Stock::join('resource_stock', 'stock.id', '=', 'resource_stock.stock_id')
                ->where('stock.id', $stock)
                ->where('resource_id', $resource)
                ->select('resource_stock.*')->first();
    }          
    
    private function saveMovement($resource, $stock, $quantity, $note=null)
    {
        $movement = new Stockmovement();
        $movement->stm_quantity = $quantity;
        $movement->stock_id = $stock;
        $movement->resource_id = $resource;
        $movement->stm_note = $note;
        $movement->save();
        return $movement;
    }
}
