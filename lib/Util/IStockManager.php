<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ArkSys\Mod\Stock\Util;

/**
 *
 * @author nodashi
 */

use ArkSys\Mod\Stock\Model\Resource;

interface IStockManager
{
    public function add($resource, $stock, $quantity);
    public function remove($resource, $stock, $quantity);
    public function current($resource, $stock=null);
}
