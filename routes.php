<?php

//json

Route::post('api/stock/list/', 
    [
        'as'=>'api.stock.list',
        'middleware'=>['api', 'auth:api'],
        'uses'=>'ArkSys\Mod\Stock\Controller\Api\StockController@getList'
    ] 
);

Route::get('api/stock/all',
    [
        'as'=>'api.stock.all',
        'middleware'=>['api', 'auth:api'],
        'uses'=>'ArkSys\Mod\Stock\Controller\Api\StockController@all'
    ] 
);

Route::get('api/stock/items/{stockId?}',
    [
        'as'=>'api.stock.items',
        'middleware'=>['api', 'auth:api'],
        'uses'=>'ArkSys\Mod\Stock\Controller\Api\StockController@items'
    ] 
);

Route::get('api/stock/{id}',
    [
        'as'=>'api.stock.detail',
        'middleware'=>['api', 'auth:api'],
        'uses'=>'ArkSys\Mod\Stock\Controller\Api\StockController@find'
    ] 
);

Route::post('api/stock/save',
    [
        'as'=>'api.stock.save',
        'middleware'=>['api', 'auth:api'],
        'uses'=>'ArkSys\Mod\Stock\Controller\Api\StockController@save'
    ] 
);

Route::post('api/stock/movement/save',
    [
        'as'=>'api.stock.movement.save',
        'middleware'=>['api', 'auth:api'],
        'uses'=>'ArkSys\Mod\Stock\Controller\Api\StockmovementController@save'
    ] 
);